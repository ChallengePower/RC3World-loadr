CLASSIFIED BY DRC MATTHEW GOSHKO FOR REASONS 1.4(B) AND (D) 
 
1. (S) SUMMARY: Najaf's Governor, the Provincial Council 
Chair and the Governor's aide all believe Grand Ayatollah 
al-Sistani's intervention in the Najaf crisis has left al- 
Sadr significantly weakened politically.  They further feel 
that the agreement al-Sadr signed has strengthened the 
Iraqi Government's ability to move against al-Sadr if the 
Mahdi Militia takes up arms once again.  Council Chair 
Shayk Fayed Kazem al-Shamri also shared an interesting 
analysis of the political make up of the Jaysh al-Mahdi 
movement and the role that certain Iranian interests may 
play in supporting it.  END SUMMARY. 
 
-------------------------------------- 
AL-SADR AND THE JAM'S POLITICAL FUTURE 
-------------------------------------- 
 
2. (S) Provincial Council Chair Shayk Fa'ad said the recent 
developments would finish al-Sadr politically and most 
likely lead to a further splintering of the Jaysh al-Mahdi 
movement.  According to Fa'ad, the Jaysh Al-Mahdi is split 
into three camps: moderates, led by clerics such as Ali 
Sismesom, extremists, led by the likes of Qais al-Khza'ali, 
and a third group led by ex-Mukhabarat officers from the 
former regime.  While moderates within the movement would 
probably try and join the Dawa party (NOTE: Fa'ad is a Dawa 
member.  END NOTE), Dawa is unlikely to accept them because 
the group as a whole is tainted by the extremists and ex- 
regime members.  Fa'ad also noted that it traditionally 
takes years of affiliation, often as much as ten, before an 
individual will be accepted into Dawa's full membership, 
let alone leadership.  The reasons for this are historical 
and ideological.  Dawa withheld full admission for long 
periods of time as a defense against infiltration from 
Saddam's security and intelligence services.  These long 
waiting periods have led to an organization that is 
politically homogenous.  The Dawa leadership is unlikely to 
accept vocal and active new members whose ideas may differ 
from those of the established leadership.  Fa'ad speculated 
that some elements of the Jaysh al-Mahdi might be absorbed 
into the Supreme Council for Islamic Revolution in Iraq 
(SCIRI). 
 
3. (S) While it is not uncommon for a Najafi Dawa party 
member to label all things evil as being somehow influenced 
by SCIRI, Fa'ad actually elaborated on his reasoning. 
Fa'ad explained that when SCIRI first rose to prominence 
after liberation, there was a schism in the party, and 
within its Badr Corps, over how closely the party should 
remain aligned to Iran.  Prominent clerics like Mohammed 
Bakir al-Hakim sought to maintain a healthy distance from 
Iran. Other members, including Badr Corps commanders such 
as Mahdi Muhandis, wanted to have closer ties with Iran. 
Fa'ad alleged that individuals like Muhandis had made early 
overtures to the Jaysh al-Mahdi movement and may now look 
to absorb these erstwhile allies, and by extension the 
resources of their Iranian backers, into SCIRI. 
 
--------------------------------- 
MONOLITHIC IRANIAN AGENDA A MYTH? 
--------------------------------- 
 
4. (S) The Jasyh al-Mahdi, Fa'ad said, receive Iranian 
support.  But, he stressed, there are actually three groups 
in Iran that act with different agendas.  These groups are 
the Revolutionary Guards, the Iranian Intelligence Service, 
and President Khatami's administration.  These three 
groups, Fa'ad said, are all afraid of each other and are 
constantly jockeying for advantage.  This maneuvering 
includes backing different players in Iraq, with the Jaysh 
al-Mahdi receiving support from the Revolutionary Guards. 
 
-------------------------- 
WHO FORCED SISTANI'S HAND? 
-------------------------- 
 
5. (S) Fa'ad seemed as surprised as everyone that Sistani 
had made the sudden, unannounced return to Najaf.  Fa'ad 
agrees with the conventional wisdom that Sistani is by 
nature a politically timid man who will not take action 
unless forced to do so.  He noted that none of the 
proposals outlined by Sistani in Basra on August 26 and 
agreed to by al-Sadr was new, having been spelled out by 
various players over the last several months.  Fa'ad told 
us he had personally visited four times with Sistani's son, 
beginning on April 7, to request that Sistani announce 
these proposals with an aim towards disbanding and 
disarming the al-Mahdi Militia.  Fa'ad was very curious to 
know what, or specifically who, had prompted Sistani to 
move so suddenly. 
 
------------ 
TALAL BILAL: 
------------ 
 
6. (S) Talal Bilal, the Governor's aide and close friend, 
shared Fa'ad's assessment that al-Sadr is finished 
politically.  Talal, who was present at the discussion 
site, said that al-Sadr arrived at the meeting, held in a 
house owned by a Hawza member, accompanied by a single aide 
who served as bodyguard and driver.  When asked if al-Sadr 
would take up arms again, Talal pointed to a copy of the 
signed agreement between al-Sadr and Sistani.  Talal said 
that if al-Sadr does violate the agreement, he would be 
pitting himself against the entire Shia world, which, Talal 
intimated, would react strongly to the transgression. 
Governor Zurufi shares Talal's assessment that al-Sadr is 
finished politically.  The Governor also agrees that if al- 
Sadr violates the points of the proposal that the 
government will be on much firmer ground to move against 
him. 
 
------- 
COMMENT 
------- 
 
7. (S) All three men agree that al-Sadr is finished 
politically.  They also believe his ability to cause 
trouble with his militia has been severely undercut. 
Embassy, however, advises caution in accepting their second 
premise.  Al-Sadr does not enjoy being without influence. 
His political marginalization may actually increase the 
chance he will once again stir up trouble with his militia. 
Embassy notes that al-Sadr has escaped with his skin twice. 
Much will hinge on whether Prime Minister Allawi carries 
out his intent to have the legal action against al-Sadr 
continue.  Furthermore, Embassy does not see evidence that 
the al-Mahdi Militia will be easily or completely disarmed. 
Reports as of August 27, including visual confirmation by 
predator drone, reveal that the al-Mahdi Militia are 
sneaking weapons from the shrine area.  Finally, it can not 
be said conclusively that those fighters who make up the 
majority of the Jasyh al-Mahdi's base, mostly unemployed 
and uneducated young men, will feel constrained by 
Sistani's opinion. 
 
8. (S) That said, al-Sadr is now deprived of his primary 
trump card--the holy mosques with their religious 
significance and "citadel" character that made it difficult 
to use coalition forces to expel the al-Mahdi militia.  He 
has suffered grievous losses among some of his better 
fighting units and was clearly maneuvered into a difficult 
position by the impromptu alliance of secular Shi'a PM 
Allawi and religious Shi'a leader Sistani.  If the 
agreement holds this will have significance throughout 
Shi'a Iraq and beyond.  END COMMENT. 
 
 
NNNN
