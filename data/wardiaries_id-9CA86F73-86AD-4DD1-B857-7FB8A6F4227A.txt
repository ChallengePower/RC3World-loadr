281940Z TF SABER MM(E)08-28F ISO BULLDAWG TIC
2007-08-28 17:40:00

At appr. 1940z, MM(E)08-28F went w/d at LZ Copenhagen to extract 1xANA KIA, 2xANA WIA, and 1xANA DETAINEE WIA. Patients were injured in rocket attack on Camp Keating (see associated report). A/C went w/u from Camp Keating at approx. 1957z. A/C were w/d at Naray at 2012z. FST provided medical care and stabilized the patients for further evacuation to FAF. A/C went w/u from Naray at 2041z en route to FAF. NFTR.
